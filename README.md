# README #

ElasticMQ Docker Image

### Tech ###

* [ElasticMQ](https://github.com/adamw/elasticmq)

### How do I get set up? ###

Installs the latest version and exposes port 9324.

 * `latest` = `0.13.2`

Run with:
```sh
$ docker run --name elasticmq -p 9324:9324 -d texoit/elasticmq
```

