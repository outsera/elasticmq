FROM openjdk:8-jre-alpine
VOLUME /tmp
COPY custom.conf /custom.conf
ADD https://s3-eu-west-1.amazonaws.com/softwaremill-public/elasticmq-server-0.13.2.jar /elasticmq.jar
RUN sh -c 'touch /elasticmq.jar'
EXPOSE 9324
ENTRYPOINT [ "sh", "-c", "java -Djava.security.egd=file:/dev/./urandom -Dconfig.file=/custom.conf -jar /elasticmq.jar" ]
